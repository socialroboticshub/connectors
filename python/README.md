## Social Interaction Cloud Connector

Connector to the Social Interaction Cloud (SIC). More information of SIC can be found here: https://socialrobotics.atlassian.net/wiki/spaces/CBSR/overview

Examples of using this connector can be found here: https://bitbucket.org/socialroboticshub/connectors/src/master/python/examples/