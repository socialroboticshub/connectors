# Installation instructions

**Note:** recommended to use a virtual environment such as conda to install the package.

To install `social_interaction_cloud` as a package, do:
```
cd python/sic
pip3 install .
```

You can now import the classes from everywhere on your machine. For example, in your own script you can use:
`from social_interaction_cloud.action import ActionRunner`.
