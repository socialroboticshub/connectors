package org.bitbucket.socialroboticshub.actions.llm;

import eis.iilang.Identifier;
import eis.iilang.Parameter;
import org.bitbucket.socialroboticshub.actions.RobotAction;

import java.util.List;

public class LLMPromptAction extends RobotAction {
    public final static String NAME = "promptLLM";

    /**
     * @param parameters A list of 3 identifiers represent the interactant ID, the
     *                   entry key and the entry data that needs to be stored.
     */
    public LLMPromptAction(final List<Parameter> parameters) {
        super(parameters);
    }

    @Override
    public boolean isValid() {
        return (getParameters().size() == 2) && (getParameters().get(0) instanceof Identifier); // 2nd arg can be anything
    }

    @Override
    public String getTopic() {
        return "llm_openai_prompt_agent";
    }

    @Override
    public String getData() {
        final String promptID = EIStoString(getParameters().get(0));
        final String promptData = EIStoString(getParameters().get(1));

        return (promptID + ";" + promptData);
    }

    @Override
    public String getExpectedEvent() {
        return null;
    }
}
